/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
import model.CorreoElectronico;
import model.Direccion;
import model.Profesor;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author Maite
 */
public class App {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner teclado = new Scanner(System.in);

        int opcion;
        Profesor p = null;
        Direccion d = null;
        Set<CorreoElectronico> correosElectronicos = new HashSet<>();
        CorreoElectronico ce1, ce2, ce3;
        //CREAMOS CONEXION
        SessionFactory sessionFactory;
        Configuration configuration = new Configuration();
        configuration.configure();
        sessionFactory = configuration.buildSessionFactory();
        SessionFactory factory = new Configuration().configure().buildSessionFactory();

        //CREAR UNA SESION
        Session session = factory.openSession();

        do {
            System.out.println("Elige opción");
            menu();
            opcion = teclado.nextInt();

            switch (opcion) {
                case 1://CREAR PROFESOR Y DIRECCIÓN
                    p = new Profesor(7, "Pepe", "Garcia", "Perez");
                    d = new Direccion(7, "Calle la Reina", 5, "Xàtiva", "Valencia");
                    ce1 = new CorreoElectronico(1, "sara@yahoo.com", p);
                    ce2 = new CorreoElectronico(2, "sara@hotmail.com", p);
                    ce2 = new CorreoElectronico(3, "sara@gmail.com", p);

                    correosElectronicos.add(ce1);
                    correosElectronicos.add(ce1);
                    correosElectronicos.add(ce1);
                    
                    p.setCorreosElectronicos(correosElectronicos);

                    p.setDireccion(d);

                    System.out.println("Profesor creado con éxito");
                    break;
                case 2://GUARDAR PROFESOR
                    session.beginTransaction();
                    session.save(p);
                    session.getTransaction().commit();
                    System.out.println("Profesor guardado con éxito");
                    break;
                case 3://LEER PROFESOR
                    p = session.get(Profesor.class, 1);
                    System.out.println(p);
                    break;
                case 4://ACTUALIZAR PROFESOR
                    session.beginTransaction();
                    p.setApe1("Rodríguez");
                    session.update(p);
                    session.getTransaction().commit();
                    System.out.println("Profesor actualizado con éxito");
                    break;
                case 5://ELIMINAR PROFESOR
                    session.beginTransaction();
                    session.delete(p);
                    session.getTransaction().commit();
                    System.out.println("Profesor eliminado con éxito");
                    break;
                case 6://SALIR
                    //cerrar la conexion
                    session.close();
                    sessionFactory.close();
                    System.out.println("Saliendo..");
                    break;
                default:
                    throw new AssertionError();
            }
        } while (opcion != 6);

    }

    public static void menu() {
        System.out.println("CONSIDERACIÓN 1. EJECUTAR LOS PASOS POR ORDEN");
        System.out.println("CONSIDERACIÓN 2. RECORDAR SALIR DE LA APLICACIÓN PARA CERRAR SESIÓN");
        System.out.println("CONSIDERACIÓN 3. SI SE INTENTA VOLVER A AÑADIR PROFESOR, COMO SON FIJOS DARÁ ERROR");
        System.out.println("1. Crear profesor");
        System.out.println("2. Guardar profesor");
        System.out.println("3. Leer profesor");
        System.out.println("4. Actualizar profesor");
        System.out.println("5. Eliminar profesor");
        System.out.println("6. Salir");
    }
}
